import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { HOST, TOKEN_AUTH_USERNAME, TOKEN_AUTH_PASSWORD, TOKEN_NAME } from './../_shared/var.constant';
@Injectable()
export class PerfilService {
    url: string = `${HOST}/oauth/token`;
    constructor( private http: HttpClient, private router: Router) { }

}
