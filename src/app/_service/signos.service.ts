import {Injectable} from '@angular/core';
import {HOST, TOKEN_NAME} from '../_shared/var.constant';
import {Subject} from 'rxjs/Subject';
import {Signos} from '../_model/signos';
import {HttpClient, HttpHeaders} from '@angular/common/http';



@Injectable()
export class SignosService {

    private url: string = `${HOST}/signos`;

    signosCambio = new Subject<Signos[]>();
    mensaje = new Subject<string>();

    constructor(private http: HttpClient) { }

    getlistarSignos() {
        let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
        return this.http.get<Signos[]>(`${this.url}/listar`, {
            headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
        });
    }

    getSignosPorId(id: number) {
        let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
        return this.http.get<Signos>(`${this.url}/listar/${id}`, {
            headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
        });
    }

    registrar(signos: Signos) {
        let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
        return this.http.post(`${this.url}/registrar`, signos, {
            headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
        });
    }

    modificar(signos: Signos) {
        let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
        return this.http.put(`${this.url}/actualizar`, signos, {
            headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
        });
    }
     eliminar(signos: Signos) {
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.http.delete(`${this.url}/eliminar/${signos.idSignos}`, {
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
    });
  }


}
