import { Component, OnInit } from '@angular/core';
import {map, startWith} from 'rxjs/operators';
import {SignosService} from '../../../_service/signos.service';
import {FormControl, FormGroup} from '@angular/forms';
import {Paciente} from '../../../_model/paciente';
import {PacienteService} from '../../../_service/paciente.service';
import {Signos} from '../../../_model/signos';
import {MatSnackBar} from '@angular/material';
import {Observable} from '../../../../../node_modules/rxjs'
import { ActivatedRoute, Params, Router } from '@angular/router';

@Component({
  selector: 'app-signos-edicion',
  templateUrl: './signos-edicion.component.html',
  styleUrls: ['./signos-edicion.component.css']
})
export class SignosEdicionComponent implements OnInit {
    myControl: FormControl = new FormControl();
    pacientes: Paciente[] = []
    signos: Signos;
    fechaSeleccionada: Date = new Date();
    maxFecha: Date = new Date();
    temperatura: string;
    pulso: string;
    ritmoRespiratorio: string;
    mensaje: string;
    filteredOptions: Observable<any[]>;
    pacienteSeleccionado: Paciente;
    edicion: boolean = false;
    id: number;
    form: FormGroup;
    constructor(private pacienteService: PacienteService, private signosService: SignosService, private route: ActivatedRoute, private router: Router, public snackBar: MatSnackBar) {
        this.form = new FormGroup({
            'id' : new FormControl(0),
            'nombre' : new FormControl('')
        });
    }

    ngOnInit() {
        this.route.params.subscribe((params: Params) => {
           this.id = params['id'];
           this.edicion = params['id'] != null;
           this.initForm();
        });
        this.listarPacientes();
        this.fechaSeleccionada.setHours(0);
        this.fechaSeleccionada.setMinutes(0);
        this.fechaSeleccionada.setSeconds(0);
        this.fechaSeleccionada.setMilliseconds(0);
        this.filteredOptions = this.myControl.valueChanges
            .pipe(
                startWith(null),
                map(val => this.filter(val))
            );
    }

    private initForm(){
        if(this.edicion){
            this.signosService.getSignosPorId(this.id).subscribe(data =>{
               let  idSignos = data.idSignos;
               let paciente = data.paciente;
               let temperatura = data.temperatura;
               let fecha = data.fecha;
               let pulso = data.pulso;
               let ritmoRespiratorio = data.ritmoRespiratorio;
               this.form = new FormGroup({
                   'idSignos': new FormControl(idSignos),
                   'paciente': new FormControl(paciente),
                   'temperatura': new FormControl(temperatura),
                   'fecha': new FormControl(fecha),
                   'pulso': new FormControl(pulso),
                   'ritmoRespiratorio': new FormControl(ritmoRespiratorio)
               });
            });
        }
    }
    filter(val: any) {
        if (val != null && val.idPaciente > 0) {
            return this.pacientes.filter(option =>
                option.nombres.toLowerCase().includes(val.nombres.toLowerCase()) || option.apellidos.toLowerCase().includes(val.apellidos.toLowerCase()) || option.dni.includes(val.dni));
        } else {
            return this.pacientes.filter(option =>
                option.nombres.toLowerCase().includes(val.toLowerCase()) || option.apellidos.toLowerCase().includes(val.toLowerCase()) || option.dni.includes(val));
        }
    }
    displayFn(val: Paciente) {
        return val ? `${val.nombres} ${val.apellidos}` : val;
    }
    seleccionarPaciente(e) {
        this.pacienteSeleccionado = e.option.value;
    }
    listarPacientes() {
        this.pacienteService.getlistar().subscribe(data => {
            this.pacientes = data;
        });
    }
    estadoBotonRegistrar() {
        return (this.pacienteSeleccionado === null);
    }
    aceptar(){
        this.signos = new Signos();
        this.signos.idSignos = this.form.value['id'];
        this.signos.paciente = this.pacienteSeleccionado;
        this.signos.fecha = this.fechaSeleccionada;
        this.signos.pulso = this.pulso;
        this.signos.ritmoRespiratorio = this.ritmoRespiratorio;
        this.signos.temperatura = this.temperatura;
        if(this.signos != null && this.signos.idSignos > 0){
            this.signosService.modificar(this.signos).subscribe(data => {
               if(data === 1) {
                   this.signosService.getlistarSignos().subscribe(signos => {
                       this.signosService.signosCambio.next(signos);
                       this.signosService.mensaje.next('Se modifico');
                   });
               } else {
                   this.signosService.mensaje.next('No se modifico');
               }
            });
        } else {
            this.signosService.registrar(this.signos).subscribe( data => {
                console.log(data);
                if(data){
                    this.snackBar.open("Se registro", "Aviso", { duration: 2000});
                }else{
                    this.snackBar.open("Error al registrar", "Aviso", { duration: 2000});
                }
            });

            setTimeout(() => {
                this.limpiarControles();
            }, 2000);
            this.router.navigate(['signos']);
        }
    }
    limpiarControles() {
        this.pacienteSeleccionado = null;
        this.pulso = null;
        this.ritmoRespiratorio = null;
        this.temperatura = null;
        this.fechaSeleccionada = new Date();
        this.fechaSeleccionada.setHours(0);
        this.fechaSeleccionada.setMinutes(0);
        this.fechaSeleccionada.setSeconds(0);
        this.fechaSeleccionada.setMilliseconds(0);
        this.mensaje = '';

    }

}
