import {Paciente} from '../../_model/paciente';
import {PacienteService} from '../../_service/paciente.service';

import { map, startWith } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { FormControl } from '@angular/forms';
import {Signos} from '../../_model/signos';
import {SignosService} from '../../_service/signos.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MatTableDataSource, MatPaginator, MatSort, MatSnackBar, MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import {DialogoPacienteComponent} from './dialogo-paciente/dialogo-paciente.component';

@Component({
  selector: 'app-signos',
  templateUrl: './signos.component.html',
  styleUrls: ['./signos.component.css']
})
export class SignosComponent implements OnInit {
    lista: Signos[] = [];
    signo: Signos;
    displayedColumns = ['idSignos', 'paciente', 'pulso', 'acciones'];
    dataSource: MatTableDataSource<Signos>;
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    cantidad: number;
  constructor(private pacienteService: PacienteService, public dialog: MatDialog, private signosService: SignosService, private snackBar: MatSnackBar, public route: ActivatedRoute) { }

  ngOnInit() {
      this.signosService.signosCambio.subscribe(data => {
         this.dataSource = new MatTableDataSource(data);
         this.dataSource.paginator = this.paginator;
         this.dataSource.sort = this.sort;
      });
      this.signosService.mensaje.subscribe(data => {
          this.snackBar.open(data, null, {
         });
      });
      this.signosService.getlistarSignos().subscribe(data => {
          this.lista = data;
          this.cantidad = JSON.parse(JSON.stringify(data)).totalElements;

          this.dataSource = new MatTableDataSource(this.lista);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
      });

      this.signosService.mensaje.subscribe(data => {
         this.snackBar.open(data, null, { duration: 2000 });
      });
  }
    applyFilter(filterValue: string) {
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
        this.dataSource.filter = filterValue;
    }

    openDialog(paciente: Paciente): void {

        let pac = paciente != null ? paciente : new Signos();
        let dialogRef = this.dialog.open(DialogoPacienteComponent, {
            width: '250px',
            disableClose: true,
            /*      data: { nombres: med.nombres , apellidos: med.apellidos, cmp: med.cmp }*/
            data: pac
        });
    }
    eliminar(signos: Signos): void {
        this.signosService.eliminar(signos).subscribe(data => {
            if (data === 1) {
                this.signosService.getlistarSignos().subscribe(medicos => {
                    this.signosService.signosCambio.next(medicos);
                    this.signosService.mensaje.next("Se elimino correctamente");
                });
            } else {
                this.signosService.mensaje.next("No se pudo eliminar");
            }
        });
    }


  }
