import { Component, OnInit, Inject } from '@angular/core';
import {Paciente} from '../../../_model/paciente';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import {PacienteService} from '../../../_service/paciente.service';

@Component({
  selector: 'app-dialogo-paciente',
  templateUrl: './dialogo-paciente.component.html',
  styleUrls: ['./dialogo-paciente.component.css']
})
export class DialogoPacienteComponent implements OnInit {

    paciente: Paciente;

    constructor(public dialogRef: MatDialogRef<DialogoPacienteComponent>,
                @Inject(MAT_DIALOG_DATA) public data: Paciente, private pacienteService: PacienteService) {
    }

    ngOnInit() {
        this.paciente = new Paciente();
        this.paciente.idPaciente = this.data.idPaciente;
        this.paciente.nombres = this.data.nombres;
        this.paciente.apellidos = this.data.apellidos;
        this.paciente.direccion = this.data.direccion;
        this.paciente.telefono = this.data.telefono;
        this.paciente.dni = this.data.dni;
    }

    operar(){
        if(this.paciente != null && this.paciente.idPaciente > 0) {
            this.pacienteService.modificar(this.paciente).subscribe(data => {
                if (data === 1) {
                    this.pacienteService.getlistar().subscribe(pacientes => {
                        this.pacienteService.pacienteCambio.next(pacientes);
                        this.pacienteService.mensaje.next("Se modifico");
                    });
                } else {
                    this.pacienteService.mensaje.next("No se pudo modificar");
                }
            });
        } else {
            this.pacienteService.registrar(this.paciente).subscribe(data => {
                if (data === 1) {
                    this.pacienteService.getlistar().subscribe(medicos => {
                        this.pacienteService.pacienteCambio.next(medicos);
                        this.pacienteService.mensaje.next("Se registro");
                    });
                } else {
                    this.pacienteService.mensaje.next("No se pudo registrar");
                }
            });
        }
        this.dialogRef.close();

    }

    cancelar(){
        this.dialogRef.close();
        this.pacienteService.mensaje.next('false');
    }


}
